git clone https://github.com/isard-vdi/moodle-auth_saml2.git /var/www/html/auth/saml2

curl --location https://moodle.org/plugins/download.php/23360/mod_jitsi_moodle310_2021020300.zip > jitsi.zip
unzip jitsi.zip -d /var/www/html/mod/
rm jitsi.zip

curl --location https://moodle.org/plugins/download.php/23294/mod_bigbluebuttonbn_moodle310_2019101004.zip > bbb.zip
unzip bbb.zip -d /var/www/html/mod/
rm bbb.zip
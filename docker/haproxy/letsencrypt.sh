RENEW=0
if [[ ! -f /certs/chain.pem && ! -z "$LETSENCRYPT_EMAIL" && ! -z "$LETSENCRYPT_DNS" ]]; then
       	/usr/bin/certbot certonly --standalone -d "$LETSENCRYPT_DNS" -m "$LETSENCRYPT_EMAIL" -n --agree-tos
        if [[ $? == 0 ]] ; then
                cat /etc/letsencrypt/live/$LETSENCRYPT_DNS/fullchain.pem /etc/letsencrypt/live/$LETSENCRYPT_DNS/privkey.pem > /certs/chain.pem
                chmod 440 /certs/chain.pem
                mkdir -p /certs/letsencrypt/$LETSENCRYPT_DNS
                cp /etc/letsencrypt/live/$LETSENCRYPT_DNS/* /certs/letsencrypt/$LETSENCRYPT_DNS/
		RENEW=1
	fi
fi

if [ $RENEW == 1 ]; then
	/bin/sh -c '/letsencrypt-check.sh' &
fi

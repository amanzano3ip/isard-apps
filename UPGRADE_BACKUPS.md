How To Upgrade bbb-docker
Backup

if you use greenlight, create a database backup first

docker exec -t docker_postgres_1 pg_dumpall -c -U postgres > /root/greenlight_`date +%d-%m-%Y"_"%H_%M_%S`.sql

Upgrading

# upgrade!
./scripts/upgrade

# restart updated services
./scripts/compose up -d


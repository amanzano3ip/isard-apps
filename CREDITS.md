[submodule "docker/nextcloud/src"]
	path = docker/nextcloud/src
	url = https://github.com/ONLYOFFICE/docker-onlyoffice-nextcloud
	branch = master
[submodule "docker/jitsi/src"]
	path = docker/jitsi/src
	url = https://github.com/jitsi/docker-jitsi-meet.git
	branch = master
[submodule "docker/cryptpad/src"]
	path = docker/cryptpad/src
	url = https://github.com/xwiki-labs/cryptpad-docker.git
	branch = master
[submodule "docker/alpha-bbb/src"]
	path = docker/alpha-bbb/src
	url = https://github.com/bigbluebutton/docker.git
[submodule "docker/bbb/src"]
	path = docker/bbb/src
	url = https://github.com/alangecker/bigbluebutton-docker.git
[submodule "docker/onlyoffice_build/build_tools"]
	path = docker/onlyoffice_build/build_tools
	url = https://github.com/ONLYOFFICE/build_tools.git
[submodule "docker/onlyoffice/src"]
	path = docker/onlyoffice/src
	url = https://github.com/aleho/onlyoffice-ce-docker-license.git
[submodule "docker/wordpress/src"]
	path = docker/wordpress/src
	url = https://github.com/nezhar/wordpress-docker-compose.git
[submodule "docker/etherpad/src"]
	path = docker/etherpad/src
	url = https://github.com/Jamesits/docker-etherpad-lite

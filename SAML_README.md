# KEYCLOAK

## Create sample users

1. https://sso.<domain>

# Moodle

1. Activate SAML2 plugin
2. Regenerate SP certificate: https://moodle.<domain>/auth/saml2/regenerate.php
3. Download SAML2 Service Provider xml: https://moodle.<domain>/auth/saml2/sp/metadata.php
4. Import this SP in keycloak IDP: https://sso.<domain>/auth/admin/master/console/#/create/client/poc
5. Add builtin email, givenname and surname field mappers (https://sso.<domain>/auth/admin/master/console/#/realms/poc/clients/b7781aac-5aa5-441a-8af5-aca7cc0a1daf/mappers)
6. Copy keycloak SAML IDP xml data (https://sso.<domain>/auth/realms/poc/protocol/saml/descriptor) to moodle (IdP metadata xml OR public xml URL: https://moodle.<domain>/admin/settings.php?section=authsettingsaml2). Note that you should copy it from a text editor, not the content from the browser view!

## Configuration

- Auto create users: Yes
- Role Mapping: Configure the same roles you created in keycloak for users (administratos, course creators and managers)
- Mapping idP: username
- Data mapping: (with this fields moodle will skip user profile fill as this three are the only required)
  - Name: givenName
  - Surname(s): sn
  - Email: email
- Dual login: No 
  - This will redirect moodle login automatically to keycloak login. To access as local moodle admin add to url: /login/index.php?saml=off

At keycloak side create a basic user property 'username' for this client mappers.

## Fields returned from keycloak

Authed!

array(3) {
  ["sn"]=>
  array(1) {
    [0]=>
    string(5) "admin"
  }
  ["givenName"]=>
  array(1) {
    [0]=>
    string(5) "admin"
  }
  ["Role"]=>
  array(6) {
    [0]=>
    string(20) "manage-account-links"
    [1]=>
    string(12) "view-profile"
    [2]=>
    string(6) "admins"
    [3]=>
    string(14) "offline_access"https://github.com/bigbluebutton/bigbluebutton/issues/9406
    [4]=>
    string(17) "uma_authorization"
    [5]=>
    string(14) "manage-account"
  }
}
IdP: https://sso.<domain>/auth/realms/poc

Groups could be added by adding the correct mapping in keycloak moodle client?

## SAML2 Plugin development environment

All this have to be done as the image doesn't let html external folder mounted as volume (image doesn't use root)

1. Start isard-apps-moodle docker with default config. Wait for moodle to be ready.
2. Enter docker and copy html to external folder:
   1. docker exec -ti isard-apps-moodle /bin/sh
   2. cd /var/www/html
   3. mkdir /var/www/moodledata/html
   4. cp -R . /var/www/moodledata/html

Now you open two terminals:

- docker exec -ti isard-apps-moodle /bin/sh
- docker logs isard-apps-moodle --follow

You can edit saml2 plugin from host (/opt/isard-office/moodle/data/html/auth/saml2) and copy it to the current html folder:

- /var/www/html/auth/saml2 $ cp -R /var/www/moodledata/html/auth/saml2/* .

When you finish developing get the new plugin code into a zip and in the correct src folder:

- cd ${DATA_FOLDER}/moodle/data/html/auth/ && zip -r <src git path>/isard-office/docker/moodle/plugins/auth_saml2.zip saml2

## SAML2 Plugin src

The modified source files are:

- auth.php (lines 570 to 595, sync_roles call added)
- locallib.php (function sync_roles)

Also the common plugin setup fields and lang strings:

- settings.php (lines 314 to 333)
- lang/en/auth_saml2.php (lines 24 to 29)



# NEXTCLOUD

TODO: Does not map email nor friendlyname (display name). Also not tested to add quota field in keycloak and map it to nextcloud.

(guide: https://janikvonrotz.ch/2020/04/21/configure-saml-authentication-for-nextcloud-with-keycloack/)

1. Get keycloak realm RSA Certificate (not public key): https://sso.<domain>/auth/admin/master/console/#/realms/poc/keys
2. Generate Nextcloud SP keys: openssl req  -nodes -new -x509  -keyout private.key -out public.cert
3. Install saml plugin. Select integrated configuration at first config page.
4. Configure at: https://nextcloud.<domain>/settings/admin/saml
   1. General
      1. Attribute to map de UID to.: username
      2. Optioinal display name...: anything
   2. Service Provider Data
      1. x509: public.key (generated before)
      2. Private key: private.key (generated before)
   3. Identity Provider Data
      1. Identifier of the IdP: https://sso.<domain>/auth/realms/poc
      2. URL target of the IdP: https://sso.<domain>/auth/realms/poc/protocol/saml
      3. URL Location of the IdP SLO request: https://sso.<domain>/auth/realms/poc/protocol/saml
      4. Public X.509 certificate: (The RSA Certificate from keycloak at step 1). 
   4. Attribute mapping
      1. email: email
      2. user groups: Role
   5. Security Settings (check only this)
      1. Signatures and encryption offered
         1. AuthnRequest
         2. logoutRequest
         3. logoutResponse
      2. Signatures and encryption required
         1. Response
         2. Assertion [Metadata of the SP will offer this info]

Now click at Download metadata XML and upload to Keycloak new client.

1. Add Client
   1. Client SAML Endpoint: https://sso.<domain>/auth/realms/poc
2. Mappers  [[ ONLY USERNAME and ROLES WORKING. Nextcloud doesn't get email]]
   1. Name: `username`
         Mapper Type: *User Property*
         Property: `username`
         Friendly Name: `username`
         SAML Attribute Name: `username`
         SAML Attribute NameFormat: *Basic*
   2. Add builtins:Name: `email`
         Mapper Type: *User Property*
         Property: `email`
         Friendly Name: `email`
         SAML Attribute Name: `email`
         SAML Attribute NameFormat: *Basic*
   3. Name: `roles`
         Mapper Type: *Role List*
         Role attribute name: `Roles`
         Friendly Name: `Roles`
         SAML Attribute NameFormat: *Basic*
         Single Role Attribute: *On*

[Not needed?]
This is in left menú Client Scopes

In addition the *Single Role Attribute* option needs to be enabled in a different section. Navigate to *Configure > Client scopes > role_list > Mappers > role_list* and toggle the *Single Role Attribute* to *On*.

https://sso.<domain>/auth/admin/master/console/#/realms/poc/client-scopes/4d5db543-f636-44d4-b250-c7fa902ab454/mappers/83dff066-9ecc-4785-8a56-b83373f1c9d2



https://nextcloud.<domain>/login?direct=1

## Debug

Edit /opt/isard-office/nextcloud/html/config/config.php

<?php
$CONFIG = array (
  'debug' => true,

# Big Blue Button

TODO:

- Audio fails with docker in iptables=false and managed by firewalld in masquerade mode. This is due to coturn that doesn't like being behind nat.
- Firewalld + BBB: As  BBB will 'take' the host interface we should:
  - Remove /etc/docker/daemon.json the iptables: false
  - firewall-cmd --zone=public --remove-interface=docker0 --permanent
  - firewall-cmd --zone=docker --add-interface=docker0 --permanent
  - Now the docker applies iptables as per container. Note that we don't have control over this from now on.
- Scalelite
- Script creation of base debian with virt-install and then replicate BBBs (partially done)

## WORDPRESS
Client Id in keycloak has to be 'php-saml' if not set at wordpress saml plugin.

1. Install OneLogin SAML plugin
2. STATUS
   1. Enable
3. IDENTITY PROVIDER SETTINGS
   1. iDp ENTITY ID: Anything you want
   1. SSO Service Url: https://sso.digitaldemocratic.net/auth/realms/poc/protocol/saml
   2. SLO Service Url: https://sso.digitaldemocratic.net/auth/realms/poc/protocol/saml
   3. X.509 Certificate: Copy the Certificate (not the Public key) from the keycloak realm (https://sso.digitaldemocratic.net/auth/admin/master/console/#/realms/poc/keys) without the begin/end lines in the cert.
4. OPTIONS
   1. Create user if not exists
   2. Update user data
   3. Force SAML login (wait till the plugin is working. We need a way to login as admin again!)
   4. Single Log Out
   5. Match Wordpress account by: username ???
5. ATTRIBUTE MAPPING
   1. Username: username
   2. Email: email
   4. First Name: givenName
   5. Last Name: sn
   6. Role: Role
6. ROLE MAPPING
   1. Administrator: admins
   2. Editor: managers
   3. Author: coursecreators
   ...
   4. Multiple role values...: true
7. CUSTOMIZE ACTIONS AND LINKS
   1. Stay in WordPress after SLO
8. ADVANCED SETTINGS
   1. Sign AuthnRequest
   2. Sign LogoutRequest
   3. Sign LogoutResponse
   4. Service Provider X.509 Certificate & Service Provider Private Key: Generate both and paste it without the begin/end lines:
         openssl req  -nodes -new -x509  -keyout private.key -out public.cert

9. Download Service Provider metadata from top and add it to keycloak
10. Keycloak client mappers:
   1. Name: `username`
         Mapper Type: *User Property*
         Property: `username`
         Friendly Name: `username`
         SAML Attribute Name: `username`
         SAML Attribute NameFormat: *Basic*
   2. Add builtins:Name: `email`
         Mapper Type: *User Property*
         Property: `email`
         Friendly Name: `email`
         SAML Attribute Name: `email`
         SAML Attribute NameFormat: *Basic*
   3. Name: `roles`
         Mapper Type: *Role List*
         Role attribute name: `Roles`
         Friendly Name: `Roles`
         SAML Attribute NameFormat: *Basic*
         Single Role Attribute: *On*